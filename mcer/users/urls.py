from django.conf.urls import url
from mcer.users.views import users_list, user_new, denied

urlpatterns = [    
    url(r'^$',users_list),
    url(r'^new$',user_new),    
    url(r'^denied$',denied),
]

