from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from mcer.users.forms import UserForm
from django.contrib.auth.decorators import user_passes_test
from mcer.utils import is_superuser

@user_passes_test(is_superuser,login_url = "/users/denied")
def users_list(request):
    return render(request, 'users/list.html', {
        'users': User.objects.all(),
        'form':UserForm(initial = {'username': '','password':''})
    })

@user_passes_test(is_superuser,login_url = "/users/denied")
def user_new(request):
    f = UserForm(request.POST)
    if f.is_valid():        
        user = User.objects.create_user(f['username'].value(), '', f['password'].value())
        user.is_staff = 1
        user.save()
    return redirect('/users/')

def denied(request):
    return render(request,'users/denied.html')