def is_superuser(user):
    return user.is_superuser

def is_staff(user):
    return user.is_staff