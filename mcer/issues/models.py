from django.db import models
from django.conf import settings

class Categories(models.Model):
    def __str__(self):
        return u'{0}'.format(self.label)
    label = models.CharField(max_length=32)

class Issues(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='owner')
    solver = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='solver')
    category = models.ForeignKey(Categories, related_name='category')
    state = models.IntegerField(choices = [(0,'New'),(1,'Working'),(2,'Finished'),(3,'Closed')])
    label = models.CharField(max_length=32)
    description = models.TextField()
    time_min = models.IntegerField()