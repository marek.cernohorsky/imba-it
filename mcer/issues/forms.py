from django import forms
from mcer.issues.models import Categories, Issues

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Categories
        fields = ['label']

class IssueForm(forms.ModelForm):
    class Meta:
        model = Issues
        fields = ['owner','solver','state','category','label','description','time_min']
        widgets = {
            'owner': forms.HiddenInput(),            
        }
