from django.conf.urls import url
from mcer.issues.views import issues_list, categories_list, category_new, issue_new, issue_detail, issue_edit, issue_save
urlpatterns = [    
    url(r'^$',issues_list),
    url(r'^new$',issue_new),
    url(r'(?P<issue_id>[0-9]+)$',issue_detail),
    url(r'(?P<issue_id>[0-9]+)/edit$',issue_edit),
    url(r'(?P<issue_id>[0-9]+)/save$',issue_save),
    url(r'^categories/$',categories_list),
    url(r'^categories/new$',category_new),
]

