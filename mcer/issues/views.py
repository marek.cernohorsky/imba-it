from django.shortcuts import render, redirect
from mcer.issues.models import Categories, Issues
from mcer.issues.forms import CategoryForm, IssueForm
from django.contrib.auth.decorators import user_passes_test
from mcer.utils import is_superuser, is_staff
from django.db.models import Min, Avg, Max

@user_passes_test(is_staff,login_url = "/login/")
def issues_list(request):
    if request.user.is_superuser:
        issues = Issues.objects.all()        
        min_time = Issues.objects.all().aggregate(Min('time_min'))
        avg_time = Issues.objects.all().aggregate(Avg('time_min'))
        max_time = Issues.objects.all().aggregate(Max('time_min'))
    else:
        issues = Issues.objects.filter(solver = request.user)
        min_time = Issues.objects.filter(solver = request.user).aggregate(Min('time_min'))
        avg_time = Issues.objects.filter(solver = request.user).aggregate(Avg('time_min'))
        max_time = Issues.objects.filter(solver = request.user).aggregate(Max('time_min'))        
    return render(request, 'issues/list.html', {
        'issues': issues,
        'min_time': min_time, 'avg_time': avg_time, 'max_time':max_time,
        'form': IssueForm(initial={'owner':request.user}),
    })

@user_passes_test(is_superuser,login_url = "/users/denied")
def issue_new(request):
    f = IssueForm(request.POST)
    if f.is_valid():
        f.save()
    return redirect('/issues/')    

@user_passes_test(is_staff,login_url = "/login")
def issue_detail(request,issue_id):
    issue = Issues.objects.get(pk = issue_id)
    if request.user.is_superuser or (issue.solver == request.user):
        return render(request, 'issues/detail.html', {
            'issue': issue,        
        })
    else:
        return render(request, 'users/denied.html')

@user_passes_test(is_superuser,login_url = "/users/denied")
def issue_edit(request,issue_id):
    return render(request, 'issues/edit.html', {
        'issue_id': issue_id,
        'form': IssueForm(instance=Issues.objects.get(pk = issue_id)),        
    })

@user_passes_test(is_superuser,login_url = "/users/denied")
def issue_save(request,issue_id):
    f = IssueForm(request.POST, instance=Issues.objects.get(pk = issue_id))
    f.save()
    return redirect('/issues/')

@user_passes_test(is_superuser,login_url = "/users/denied")
def categories_list(request):
    return render(request, 'issues/categories.html', {
        'categories': Categories.objects.all(),
        'form':CategoryForm()
    })

@user_passes_test(is_superuser,login_url = "/users/denied")
def category_new(request):
    f = IssueForm(request.POST)    
    if f.is_valid():
        f.save()
    return redirect('/issues/categories')
    